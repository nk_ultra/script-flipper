;	Script Flipper: an attempt at an expert system that identifies an unknown language script from given data
;	Noah Kuhn
;	8 April 2021

;	Built on top of TMYCIN, templated after:

;  snakes.lsp          Gordon Novak       15 June 88; 11 Feb 96; 06 Nov 20
; Copyright (c) 1988 by Gordon S. Novak Jr.
; This program may be freely copied, used, or modified,
; provided that this copyright notice is included in each
; copy of the code or parts thereof.

(load "tmycin.lsp")

(defcontext 'script
	'(	; parameters
		(name	; this is the list of scripts we support
			(
				arabic
				cherokee
				cyrillic
				;devanagari	; not supported
				;geez		; not supported
				greek
				;hangul		; not supported
				hebrew
				;inuktitut	; not supported
				latin
				mongolian
				nko
				;thai		; not supported
			)
		)
		(language string)

		; parameters with options
		(type (abjad abugida alphabet logography syllabary other))
		(location (africa americas c-asia europe oceania s-asia se-asia sw-asia)
			("Where was this writing script found?")
		)
		(medium (calligraphy carved digital handwritten printed)
			("How was this writing script actually written?"
			"calligraphy = brush, quill, or fountain pen on"
			"              parchment with paint or ink"
			"carved      = carved or pressed into a surface"
			"digital     = rendered on a digital screen, not"
			"              including images of other media"
			"handwritten = written in ink or graphite with"
			"              modern utensils like pen or pencil"
			"printed     = standardized glyphs printed on any"
			"              surface, including clothing,"
			"              sculpted glyphs, and signage")
		)
		(style (angular curvy loopy pictorial)
			("How do the writing system's glyphs appear?"
			"pictorial = glyphs convey detailed images"
			"angular   = glyphs consist of only straight lines"
			"curvy     = glyphs consist of >=1 curved line"
			"loopy     = glyphs consist of >=1 curved line"
			"            that closes a loop by itself"
			"Answer in estimated proportion of glyphs"
			"fitting each option's criteria:"
			"((angular <#angular/total>) ...)")
		)
		(orientation (ltr rtl vertical)
			("What is the orientation of the script?"
			"ltr        = text written left-to-right"
			"rtl        = text written right-to-left"
			"vertical   = text written vertically")
		)

		; the following two parameters may be added in the future:
		;; (thickness (thick thin varies)
		;; 	("How thick are the glyphs written?")
		;; )
		;; (diacritics (none few some many)
		;; 	("How many diacritic marks are present?"
		;; 	"none = no diacritic marks at all"
		;; 	"few  = >=10 glyphs per diacritic mark"
		;; 	"some = <10, >=5 glyphs per diacritic mark"
		;; 	"many = <5 glyphs per diacritic mark")
		;; )

		; boolean parameters
		(complex nil 
			("Does the average glyph contain over three"
			"differentiable lines OR more than one diacritic"
			"mark? Answer yes and no with the estimated"
			"proportion of glyphs fitting this criterion:"
			"(yes <#complex/total>)")
		)
		(letter-cases nil
			("Are there upper- and lower-case glyphs?")
		)
		(connected nil
			("Are many glyphs connected to each other?")
		)
	)
	'(medium location style orientation connected)	; Initial data
	'(name language type letter-cases)				; Goals
)

(defrules

;----------------------------------------------------------------
; First we have a lot of rules to conclude exact names
; based on good data, assuming it is available.
;----------------------------------------------------------------

;a check for Mongolian script
(rule-mongolian
	($and
		(same cntxt orientation vertical)
		(notsame cntxt orientation ltr)
		(notsame cntxt orientation rtl)
		(same cntxt connected yes)
	)
	;Mongolian script is always written vertically with connected
	;glyphs. It originates in Central Asia, but might be found
	;anywhere.
	(conclude cntxt name mongolian tally 900)
)

;a check for super-low complexity scripts
(rule-superlow-complexity
	(lesseq* (val1 cntxt complex) 0.06)
	(do-all
		(conclude cntxt name hebrew tally 500)	; Hebrew glyphs have complexity 0
		(conclude cntxt name greek tally 400)	; Greek glyphs have complexity 0.04
		(conclude cntxt name latin tally 200)	; Latin glyphs have complexity 0.08
	)
)

;a check for low complexity scripts
(rule-low-complexity
	($and
		(greateq* (val1 cntxt complex) 0.04)
		(lesseq* (val1 cntxt complex) 0.14)
	)
	(do-all
		(conclude cntxt name greek tally 400)	; Greek glyphs have complexity 0.04
		(conclude cntxt name latin tally 400)	; Latin glyphs have complexity 0.08
		(conclude cntxt name cherokee tally 300); Cherokee glyphs have complexity 0.14
		(conclude cntxt name nko tally 200)		; N'ko glyphs have complexity 0.15
	)
)

;a check for mid complexity scripts
(rule-mid-complexity
	($and
		(greateq* (val1 cntxt complex) 0.1)
		(lesseq* (val1 cntxt complex) 0.18)
	)
	(do-all
		(conclude cntxt name latin tally 200)	; Latin glyphs have complexity 0.08
		(conclude cntxt name cherokee tally 400); Cherokee glyphs have complexity 0.14
		(conclude cntxt name nko tally 400)		; N'ko glyphs have complexity 0.15
		(conclude cntxt name arabic tally 300)	; Arabic glyphs have complexity 0.18
		(conclude cntxt name cyrillic tally 200); Cyrillic glyphs have complexity 0.21
	)
)

;a check for high complexity scripts
(rule-high-complexity
	(greateq* (val1 cntxt complex) 0.19)
	(do-all
		(conclude cntxt name arabic tally 200)	; Arabic glyphs have complexity 0.18
		(conclude cntxt name cyrillic tally 500); Cyrillic glyphs have complexity 0.21
	)
)

; a check for non-angular scripts
(rule-not-angular
	(notsame cntxt style angular)
	(do-all
		; first, believe non-angular scripts
		(conclude cntxt name arabic tally 300)		; Arabic glyphs have angularity 0.04
		(conclude cntxt name cherokee tally 150)	; Cherokee glyphs have angularity 0.24
		(conclude cntxt name hebrew tally 150)		; Hebrew glyphs have angularity 0.26
		; now to doubt very yes-angular scripts
		(conclude cntxt name cyrillic tally -200)	; Cyrillic glyphs have angularity 0.5
		(conclude cntxt name nko tally -400)		; N'ko glyphs have angularity 0.7
	)
)

; a check for angular scripts
(rule-yes-angular
	(same cntxt style angular)
	(do-all
		; first, believe angular scripts
		(conclude cntxt name nko tally 150)			; N'ko glyphs have angularity 0.7
		(conclude cntxt name cyrillic tally 150)	; Cyrillic glyphs have angularity 0.5
		(conclude cntxt name greek tally 150)		; Greek glyphs have angularity 0.49
		(conclude cntxt name latin tally 150)		; Latin glyphs have angularity 0.44
		; now to doubt very non-angular scripts
		(conclude cntxt name arabic tally -200)		; Arabic glyphs have angularity 0.04
	)
)

; a check for non-curvy scripts
(rule-not-curvy
	(notsame cntxt style curvy)
	(do-all
		; first, believe non-curvy scripts
		(conclude cntxt name nko tally 200)			; N'ko glyphs have curviness 0.19
		; now to doubt very yes-curvy scripts
		(conclude cntxt name cyrillic tally -200)	; Cyrillic glyphs have curviness 0.54
		(conclude cntxt name cherokee tally -200)	; Cherokee glyphs have curviness 0.56
		(conclude cntxt name latin tally -200)		; Latin glyphs have curviness 0.6
		(conclude cntxt name arabic tally -300)		; Arabic glyphs have curviness 0.75
		(conclude cntxt name hebrew tally -500)		; Hebrew glyphs have curviness 0.93
	)
)

; a check for curvy scripts
(rule-yes-curvy
	(same cntxt style curvy)
	(do-all
		; first, believe curvy scripts
		(conclude cntxt name hebrew tally 150)		; Hebrew glyphs have curviness 0.93
		(conclude cntxt name arabic tally 150)		; Arabic glyphs have curviness 0.75
		(conclude cntxt name latin tally 150)		; Latin glyphs have curviness 0.6
		(conclude cntxt name cherokee tally 150)	; Cherokee glyphs have curviness 0.56
		(conclude cntxt name cyrillic tally 150)	; Cyrillic glyphs have curviness 0.54
		(conclude cntxt name greek tally 150)		; Greek glyphs have curviness 0.47
		; no very non-curvy scripts
	)
)

; a check for non-loopy scripts
(rule-not-loopy
	(notsame cntxt style loopy)
	(do-all
		; first, believe non-loopy scripts
		(conclude cntxt name hebrew tally 150)		; Hebrew glyphs have loopiness 0.04
		(conclude cntxt name cyrillic tally 150)	; Cyrillic glyphs have loopiness 0.14
		(conclude cntxt name latin tally 150)		; Latin glyphs have loopiness 0.21
		; now to doubt very yes-loopy scripts
		(conclude cntxt name greek tally -100)		; Greek glyphs have loopiness 0.29
		(conclude cntxt name arabic tally -200)		; Arabic glyphs have loopiness 0.32
	)
)

; a check for loopy scripts
(rule-yes-loopy
	(same cntxt style loopy)
	(do-all
		; first, believe loopy scripts
		(conclude cntxt name arabic tally 150)		; Arabic glyphs have loopiness 0.32
		(conclude cntxt name greek tally 150)		; Greek glyphs have loopiness 0.29
		(conclude cntxt name cherokee tally 150)	; Cherokee glyphs have loopiness 0.27
		(conclude cntxt name nko tally 150)			; N'ko glyphs have loopiness 0.26
		; now to doubt very non-loopy scripts
		(conclude cntxt name cyrillic tally -200)	; Cyrillic glyphs have loopiness 0.14
		(conclude cntxt name hebrew tally -500)		; Hebrew glyphs have loopiness 0.04
	)
)

; ---------------------------------------------------------------
; LOW-INFO RULES: good data is not available, but we still want
;                 to conclude some useful information
; ---------------------------------------------------------------

;vertical orientation: tip scales toward vertical scripts
(rule-orientation-vert
	(same cntxt orientation vertical)
	(do-all
		(conclude cntxt name mongolian tally 500)
		; don't actively doubt any other scripts, as horizontal
		; scripts can become vertical on signs or scrolls
	)
)

;non-vertical orientation: tip scales away from vertical scripts
(rule-orientation-horiz
	(thoughtnot cntxt orientation vertical)
	(conclude cntxt name mongolian tally -500)
)

;ltr orientation: tip scales away from vertical + rtl scripts
(rule-orientation-ltr
	($and
		(same cntxt orientation ltr)
		(notsame cntxt orientation vertical)
	)
	(do-all
		;doubt mongolian the most, as it is a vertical script
		(conclude cntxt name mongolian tally -600)
		;these scripts are rtl
		(conclude cntxt name arabic tally -200)
		(conclude cntxt name hebrew tally -200)
		(conclude cntxt name nko tally -200)
		;and these are ltr
		(conclude cntxt name cherokee tally 300)
		(conclude cntxt name cyrillic tally 300)
		(conclude cntxt name greek tally 300)
		(conclude cntxt name latin tally 300)
	)
)

;rtl orientation: tip scales away from vertical + ltr scripts
(rule-orientation-rtl
	($and
		(same cntxt orientation rtl)
		(notsame cntxt orientation vertical)
	)
	(do-all
		;doubt mongolian the most, as it is a vertical script
		(conclude cntxt name mongolian tally -600)
		;these scripts are ltr
		(conclude cntxt name cherokee tally -200)
		(conclude cntxt name cyrillic tally -200)
		(conclude cntxt name greek tally -200)
		(conclude cntxt name latin tally -200)
		;and these are rtl
		(conclude cntxt name arabic tally 300)
		(conclude cntxt name hebrew tally 300)
		(conclude cntxt name nko tally 300)
	)
)

; ---------------------------------------------------------------
; SELF-REFERENCING RULES: these rules are used to increase the CF
;                         of certain parameters given implication
;                         from other parameters
; ---------------------------------------------------------------

;check if we should extra-trust the connected value given the media
(rule-trust-connected
	($and
		(same cntxt connected yes)
		($or
			(same cntxt media printed)
			(same cntxt media carved)
			(same cntxt media digital)
		)
		;these media make it slightly more difficult to connect
		;letters, so if the letters are connected while using
		;these media, we know there's a good chance the writing
		;system requires glyphs to be connected
	)
	(conclude cntxt connected yes tally 600)
)

;opposite of the previous rule
(rule-trust-disconnected
	($and
		(thoughtnot cntxt connected yes)
		($or
			(same cntxt media handwritten)
			(same cntxt media calligraphy)
		)
		;these media lend themselves to cursive and otherwise
		;connected letters fairly easily, so if our glyphs aren't
		;connected here, there's a good chance the writing system
		;doesn't connect them at all
	)
	(conclude cntxt connected yes tally -600)
)

;check if we can extra-trust the conclusion given the location
(rule-trust-africa
	($and
		(same cntxt location africa)
		($or
			(same cntxt name arabic)
			(same cntxt name nko)
		)
	)
	(conclude cntxt location africa tally 700)
)

;same as above, but americas
(rule-trust-americas
	($and
		(same cntxt location americas)
		($or
			(same cntxt name cherokee)
			(same cntxt name latin)
		)
	)
	(do-all
		(conclude cntxt location americas tally 600)
		(conclude cntxt orientation rtl tally -300)
	)
)

;same as above, but c-asia
(rule-trust-c-asia
	($and
		(same cntxt location c-asia)
		($or
			(same cntxt name cyrillic)
			(same cntxt name mongolian)
		)
	)
	(conclude cntxt location c-asia tally 600)
)

;same as above, but europe
(rule-trust-europe
	($and
		(same cntxt location europe)
		($or
			(same cntxt name cyrillic)
			(same cntxt name greek)
			(same cntxt name latin)
		)
	)
	(do-all
		(conclude cntxt location europe tally 800)
		(conclude cntxt orientation rtl tally -500)
	)
)

;same as above, but sw-asia
(rule-trust-sw-asia
	($and
		(same cntxt location sw-asia)
		($or
			(same cntxt name arabic)
			(same cntxt name hebrew)
		)
	)
	(do-all
		(conclude cntxt location sw-asia tally 800)
		(conclude cntxt orientation ltr tally -500)
	)
)

;disambiguate script, Cyrillic edition
(rule-trust-cyrillic
	($and
		(same cntxt name cyrillic)
		(greateq* (val1 cntxt complex) 0.12)
	)
	(conclude cntxt name cyrillic tally 700)
)

;if our letters are not connected, we can make some guesses:
(rule-disconnected
	(thoughtnot cntxt connected yes)
	(do-all
		(conclude cntxt name nko tally -600)
		(conclude cntxt name mongolian tally -600)
		(conclude cntxt name arabic tally -600)

		(conclude cntxt name cherokee tally 200)
		(conclude cntxt name cyrillic tally 200)
		(conclude cntxt name greek tally 200)
		(conclude cntxt name hebrew tally 200)
		(conclude cntxt name latin tally 200)
	)
)

; ---------------------------------------------------------------
; CONCLUSIONS: once we have a guess at the writing system's name,
;              we get some extra information for free (assuming
;              our guess is correct)
; ---------------------------------------------------------------

;Arabic
(rule-arabic-lang
	(same cntxt name arabic)
	(conclude cntxt language "Arabic" tally 1000)
)

(rule-arabic-type
	(same cntxt name arabic)
	(conclude cntxt type abjad tally 900)
	; Arabic script is an "impure" abjad, as there are ways of signifying
	; vowels in it (pure abjads can do not signify vowels)
)

(rule-arabic-cases
	(same cntxt name arabic)
	(conclude cntxt letter-cases yes tally -1000)
)

;Cherokee
(rule-cherokee-lang
	(same cntxt name cherokee)
	(conclude cntxt language "Cherokee" tally 1000)
)

(rule-cherokee-type
	(same cntxt name cherokee)
	(conclude cntxt type syllabary tally 1000)
)

(rule-cherokee-cases
	(same cntxt name cherokee)
	(conclude cntxt letter-cases yes tally 500)
	; modern Cherokee uses enlarged versions of the glyphs as uppercase
	; originally, Cherokee did not do this
)

;Cyrillic
(rule-cyrillic-lang
	(same cntxt name cyrillic)
	(conclude cntxt language "Russian, Ukrainian, Belarusian, and many more" tally 1000)
)

(rule-cyrillic-type
	(same cntxt name cyrillic)
	(conclude cntxt type alphabet tally 900)
	; Cyrillic is an "impure" alphabet, as some glyphs are signify
	; information on how others are pronounced (ex: soft sign)
)

(rule-cyrillic-cases
	(same cntxt name cyrillic)
	(conclude cntxt letter-cases yes tally 1000)
)

;Greek
(rule-greek-lang
	(same cntxt name greek)
	(conclude cntxt language "Greek" tally 1000)
)

(rule-greek-type
	(same cntxt name greek)
	(conclude cntxt type alphabet tally 1000)
)

(rule-greek-cases
	(same cntxt name greek)
	(conclude cntxt letter-cases yes tally 1000)
	; fun fact: the letter sigma actually has two lower-case forms
	; this does not affect the Greek letter-cases tally (still true)
)

;Hebrew
(rule-hebrew-lang
	(same cntxt name hebrew)
	(conclude cntxt language "Hebrew, Yiddish, Ladino, Mozarabic" tally 1000)
)

(rule-hebrew-type
	(same cntxt name hebrew)
	(conclude cntxt type abjad tally 900)
	; Hebrew script is an "impure" abjad, as there are ways of signifying
	; vowels in it (pure abjads can do not signify vowels)
)

(rule-hebrew-cases
	(same cntxt name hebrew)
	(conclude cntxt letter-cases yes tally -1000)
)

;Latin
(rule-latin-lang
	(same cntxt name latin)
	(conclude cntxt language "English, Spanish, French, and many more" tally 1000)
)

(rule-latin-type
	(same cntxt name latin)
	(conclude cntxt type alphabet tally 900)
	; The Latin alphabet is an alphabet, but since Latin script is so
	; widely-used, in some contexts it is used as an "impure" alphabet
)

(rule-latin-cases
	(same cntxt name latin)
	(conclude cntxt letter-cases yes tally 1000)
)

;Mongolian
(rule-mongolian-lang
	(same cntxt name mongolian)
	(conclude cntxt language "Mongolian, Manchu, Daur, Evenki" tally 1000)
)

(rule-mongolian-type
	(same cntxt name mongolian)
	(conclude cntxt type alphabet tally 1000)
)

(rule-mongolian-cases
	(same cntxt name mongolian)
	(conclude cntxt letter-cases yes tally -1000)
)

;N'Ko
(rule-nko-lang
	(same cntxt name nko)
	(conclude cntxt language "N'Ko, Mandingo, Maninka, Bambara, Dyula" tally 1000)
)

(rule-nko-type
	(same cntxt name nko)
	(conclude cntxt type alphabet tally 1000)
)

(rule-nko-cases
	(same cntxt name nko)
	(conclude cntxt letter-cases yes tally -1000)
)

)
