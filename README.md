# Script Flipper
An attempt at an expert system that identifies an unknown language script by Noah Kuhn

![A color-coded map of the world with various language scripts overlaid on the areas in which they are used. Source: Wikipedia](scripts-map.png)

## Scripts in the system
Script Flipper currently has the following scripts in the system:
- Arabic
- Cherokee
- Cyrillic
- Greek
- Hebrew
- Latin
- Mongolian
- N'Ko

Support was originally going to be added for the following scripts, as well:
- Devanagari
- Ge'ez
- Hangul
- Inuktitut Syllabics
- Thai

The following scripts are up for consideration, but may not get support:
- Anglo-Saxon Runes
- Armenian
- Batak
- Bengali
- Cuneiform
- Egyptian Hieroglyphs
- Emoji
- Georgian
- Hanuno'o
- Hiragana
- Javanese
- Katakana
- Lontara'
- Malayalam
- Math
- Maya
- Ogham
- Phoenician
- Simplified Chinese
- Sinhala
- Sundanese
- Tamil
- Tifinagh
- Traditional Chinese
- Vai


## Scripts as data
The information the system keeps about each script is organized into the following categories:

- Open-ended string categories
    - NAME (\[string\])
        - The NAME is probably what you're looking for: it represents what this writing system is called.
    - LANGUAGE (\[string\])
        - The LANGUAGE (often, languages) is the language that employ this writing system
- Categories of sets of options
    - TYPE (ABJAD, ABUGIDA, ALPHABET, LOGOGRAPHY, SYLLABARY, OTHER)
        - The TYPE is the category of writing system this is
    - LOCATION (AFRICA, AMERICAS, EUROPE, OCEANIA, S-ASIA, SE-ASIA, SW-ASIA)
        - The LOCATION (often, locations) is where this writing system is found
    - MEDIUM (CALLIGRAPHY, CARVED, DIGITAL, HANDWRITTEN, PRINTED)
        - The MEDIUM (often, media) is the way this writing system is written out (often, all of them, but slightly weighted toward the origin medium of each system)
    - STYLE (ANGULAR, CURVY, LOOPY, PICTORIAL)
        - The STYLE (often, styles) is the appearance of the writing system's glyphs, according to the below section
    - ORIENTATION (LTR, RTL, VERTICAL)
        - The ORIENTATION is the way in which this script is written
- Boolean categories
    - COMPLEX (YES, NO)
        - COMPLEX describes the complexity of the average character, according to the below section
    - LETTER-CASES (YES, NO)
        - LETTER-CASES describes whether the writing system employs letter cases
    - CONNECTED (YES, NO)
        - CONNECTED describes whether many glyphs are connected to each other
- Categories that could be added in the future
    - THICKNESS (THICK, THIN, VARIES)
        - The THICKNESS is the thickness of the writing system's glyphs
    - DIACRITICS (NONE, FEW, SOME, MANY)
        - The DIACRITICS is how many diacritics (that is, disconnected smaller markings) the writing system uses around its glyphs

### Desubjectifying criteria
The following rules explain how to desubjectify the STYLE and COMPLEX categories:

- STYLE (found on a per-glyph basis, excluding dots and diacritics; relevant options weighted with the appropriate proportion of glyphs)
    - If the glyph conveys a detailed picture: PICTORIAL
    - If the glyph consists of at least one curve that closes a loop: LOOPY
    - If the glyph consists of at least one curved line: CURVY
    - If the glyph consists of straight lines: ANGULAR
- COMPLEX (found on a per-glyph basis; relevant options weighted with the appropriate proportion of glyphs)
    - If the glyph (excluding diacritics) can be drawn with three or fewer differentiable lines and has at most one diacritic mark: NO
    - Otherwise: YES
